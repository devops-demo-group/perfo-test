package com.jj

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class UniformTest extends Simulation {

  val httpConf = http
    .baseUrl("http://localhost:8081")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("0en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

  val sleep_time = System.getProperty("sleep_time")
  val concurent_users = System.getProperty("concurent_users").toInt
  val test_duration = System.getProperty("test_duration").toInt
  val throttle_rps = System.getProperty("throttle_rps").toInt
  
  val scn = scenario("uniform")
    .exec(http("uniform_request_1")
      .get("/uniform?latency=" + sleep_time))

  setUp(
    scn.inject(
        constantConcurrentUsers(concurent_users) during (test_duration seconds)
        )
        .throttle(
            //reachRps(100) in (1 seconds),
            jumpToRps(throttle_rps),
            holdFor(test_duration seconds)
        )
    ).protocols(httpConf)
}