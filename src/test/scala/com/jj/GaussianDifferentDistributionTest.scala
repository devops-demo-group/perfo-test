package com.jj

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class GaussianDifferentDistributionsTest extends Simulation {

  val httpConf = http
    .baseUrl("http://localhost:8080")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("0en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

  val mean_sleep_time = 500
  val sd_sleep_time = 100
  val concurent_users = 800
  val test_duration = 60

  val scn1 = scenario("gaussian-100rps")
    .exec(http("gaussian_request_1")
      .get("/gaussian?mean_latency=" + mean_sleep_time + "&sd_latency=" + sd_sleep_time))

  val scn2 = scenario("gaussian-200rps")
    .exec(http("gaussian_request_2")
      .get("/gaussian?mean_latency=" + mean_sleep_time + "&sd_latency=" + sd_sleep_time))

  setUp(
    scn1.inject(
      constantConcurrentUsers(concurent_users) during (test_duration seconds))
      .throttle(
        jumpToRps(100),
        holdFor(test_duration seconds)),

      scn2.inject(
        constantConcurrentUsers(concurent_users) during (test_duration seconds))
        .throttle(
          jumpToRps(200),
          holdFor(test_duration seconds))).protocols(httpConf)
}