/***
 * Code from: https://gatling.io/2018/10/04/gatling-3-closed-workload-model-support/
 */


package com.jj

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class UniformRampUpUsersTest extends Simulation {

  val httpConf = http
    .baseUrl("http://localhost:8080")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("0en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

  val sleep_time = System.getProperty("sleep_time")
  val concurent_users = System.getProperty("concurent_users").toInt
  val test_duration = System.getProperty("test_duration").toInt
  val throttle_rps = System.getProperty("throttle_rps").toInt

  val scn = scenario("uniform")
    .exec(http("uniform_request_1")
      .get("/uniform?latency=" + sleep_time))

  /*setUp(
    scn.inject(
          rampUsersPerSec(0) to concurent_users during (test_duration seconds))
        ).maxDuration(test_duration minutes).protocols(httpConf)*/
      
    setUp(  
      scn.inject(
        incrementConcurrentUsers(5)
          .times(70)
          .eachLevelLasting(10 seconds)
          .separatedByRampsLasting(10 seconds)
          .startingFrom(0)
      ).protocols(httpConf)
    )
      
  /*setUp(
      scn.inject(
        incrementConcurrentUsers(5)
          .times(50)
          .eachLevelLasting(10 seconds)
          .separatedByRampsLasting(10 seconds)
          .startingFrom(0)
         )
    ).protocols(httpConf)*/
}